variable "app_insights" {
  description = "Switch to create appinsigts"
  default     = false
}

variable "subscribe" {
  default = false
}

variable "tags" {
  description = "A map of the tags to use for the resources that are deployed"
  type        = map(string)

  default = {
    environment = ""
  }
}

variable "interfaces" {
  description = "List of Map data for each network profile"
}

variable "resource_group_name" {
  description = "The name of the resource group in which the resources will be created"
  type        = string
}

variable "storage_resource_group_name" {
  description = "The name of the resource group of existing storage account"
  type        = string
  default     = ""
}

variable "admin_username" {
  description = "User name to use as the admin account on the VMs that will be part of the VM Scale Set"
  type        = string
  default     = "fwadmin"
}

variable "admin_password" {
  description = "Default password for admin account"
  type        = string
  default     = "Paloalto1234"
}

variable "location" {
  description = "examples: (eastus | eastus2 | centralus)"
  type        = string
  default     = ""
}

variable "fw_publisher" {
  type    = string
  default = "paloaltonetworks"
}

variable "fw_sku" {
  type    = string
  default = "byol"
}

variable "fw_series" {
  type    = string
  default = "vmseries1"
}

variable "fw_version" {
  type    = string
  default = "latest"
}

variable "fw_size" {
  type    = string
  default = "Standard_D3_v2"
}

variable "bootstrap_dir" {
  description = "Bootstrap root directory name, not required."
  type        = string
  default     = ""
}

variable "storage_account" {
  description = "Storage account name used to bootstrap VMSS instances"
  type        = string
  default     = ""
}

variable "file_share" {
  default = ""
}

variable "scale_set_name" {
  default = ""
}

variable "instance_count" {
  default = 1
}
variable "computer_name_prefix" {
  type    = string
  default = ""
}


variable "ssh_key" {
}

variable "bootstrap" {
  type        = bool
  default     = false
  description = "Enables bootstraping"
}

variable "bs_access_key" {
  default = ""
}

variable "bs_storage_account" {
  default = ""
}

variable "scale_set_rules" {}
variable "vmss_default_cap" {}
variable "vmss_min_cap" {}
variable "vmss_max_cap" {}
variable "email_notify" {}
variable "module_depends_on" {
  default = [""]
}
