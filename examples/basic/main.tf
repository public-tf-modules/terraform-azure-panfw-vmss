provider "azurerm" {
  version = "=2.0.0"
  features {}
}

resource "azurerm_resource_group" "test-rg" {
  name     = var.rg_name
  location = var.location

  tags = {
    environment = "tf-ci"
  }
}

resource "azurerm_virtual_network" "test-vnet" {
  name                = var.vnet_name
  location            = azurerm_resource_group.test-rg.location
  resource_group_name = azurerm_resource_group.test-rg.name
  address_space       = ["10.0.0.0/16"]
}

resource "azurerm_subnet" "private-subnet" {
  name                 = "private-${var.subnet_name}"
  resource_group_name  = azurerm_resource_group.test-rg.name
  virtual_network_name = azurerm_virtual_network.test-vnet.name
  address_prefix       = "10.0.2.0/24"
}

resource "azurerm_subnet" "public-subnet" {
  name                 = "public-${var.subnet_name}"
  resource_group_name  = azurerm_resource_group.test-rg.name
  virtual_network_name = azurerm_virtual_network.test-vnet.name
  address_prefix       = "10.0.1.0/24"
}

resource "azurerm_subnet" "mgmt-subnet" {
  name                 = "mgmt-${var.subnet_name}"
  resource_group_name  = azurerm_resource_group.test-rg.name
  virtual_network_name = azurerm_virtual_network.test-vnet.name
  address_prefix       = "10.0.0.0/24"
}

module "fw-ss" {
  source               = "../../"
  location             = var.location
  ssh_key              = var.ssh_key
  resource_group_name  = azurerm_resource_group.test-rg.name
  computer_name_prefix = var.computer_name_prefix
  scale_set_name       = var.scale_set_name
  interfaces = [
    {
      name          = "mgmt"
      index         = 1
      ip_forwarding = false
      subnet_id     = azurerm_subnet.mgmt-subnet.id
    },
    {
      index         = 2
      name          = "private"
      subnet_id     = azurerm_subnet.private-subnet.id
      ip_forwarding = true
    },
    {
      name          = "public"
      index         = 0
      primary       = true
      subnet_id     = azurerm_subnet.public-subnet.id
      ip_forwarding = true
    }
  ]
  email_notify     = var.email_notify
  vmss_default_cap = 2
  vmss_min_cap     = 2
  vmss_max_cap     = 4
  scale_set_rules = {
    "Percentage CPU" = {
      scale_out_threshold = 5,
      scale_in_threshold  = 20
    }
  }
}
